<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Point extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['point_get']['limit'] = 500; // 500 requests per hour per user/key
    		$this->load->helper('url');
    		$this->load->helper(array('form', 'url'));
        $this->load->model('poin_model','',TRUE);
    }

  public function point_get(){
		$id = $this->uri->segment(4);
    if(isset($id))
    {
        $data =  $this->poin_model->get_by(array('id'=>$id));
        if(isset($data['id'])){
          $this->response(array('status'=>'success','message'=>$data));
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
    }else{
      $data =  $this->poin_model->get_all();
      $this->response(array('status'=>'success','message'=>$data));
    }
	}

}
