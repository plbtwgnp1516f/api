-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2016 at 09:54 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `plbtw`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bencana`
--

CREATE TABLE `tbl_bencana` (
  `id_laporan` int(11) NOT NULL,
  `waktu` datetime NOT NULL,
  `username` varchar(20) NOT NULL,
  `penyebab` varchar(100) NOT NULL,
  `korban` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `total_kerugian` double NOT NULL,
  `tingkat_kerusakan` varchar(30) NOT NULL,
  `jenis_bencana` varchar(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `kota` varchar(30) NOT NULL,
  `negara` varchar(30) NOT NULL,
  `desa` varchar(30) NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `kecamatan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_bencana`
--

INSERT INTO `tbl_bencana` (`id_laporan`, `waktu`, `username`, `penyebab`, `korban`, `status`, `total_kerugian`, `tingkat_kerusakan`, `jenis_bencana`, `alamat`, `kota`, `negara`, `desa`, `kelurahan`, `kecamatan`) VALUES
(1, '2016-05-15 20:48:46', 'admin', 'mboh', 1, 'selesai', 2, 'sedang', 'banjir', 'jalan maguwo harjo', 'yogyakarta', 'indonesia', 'ngemplak', 'sleman', 'mboh'),
(2, '2016-05-17 23:50:45', 'admin', '123', 213, 'belum dicek', 0, 'Sedang', 'Gempa Bumi', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hadiah`
--

CREATE TABLE `tbl_hadiah` (
  `id_hadiah` int(11) NOT NULL,
  `nama_hadiah` varchar(50) NOT NULL,
  `poin` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengguna`
--

CREATE TABLE `tbl_pengguna` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(75) NOT NULL,
  `nomor_handphone` varchar(20) NOT NULL,
  `poin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pengguna`
--

INSERT INTO `tbl_pengguna` (`username`, `password`, `nama`, `email`, `nomor_handphone`, `poin`) VALUES
('admin', 'admin', 'admin', 'admin', '0', 0),
('qew', 'qwe', 'qe', 'android.support.v7.widget.AppCompatEditText{a66c8a78 VFED..CL ......ID 21,2', 'android.support.v7.w', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `id_transaksi` int(11) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `id_hadiah` int(11) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_bencana`
--
ALTER TABLE `tbl_bencana`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `tbl_hadiah`
--
ALTER TABLE `tbl_hadiah`
  ADD PRIMARY KEY (`id_hadiah`);

--
-- Indexes for table `tbl_pengguna`
--
ALTER TABLE `tbl_pengguna`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_bencana`
--
ALTER TABLE `tbl_bencana`
  MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_hadiah`
--
ALTER TABLE `tbl_hadiah`
  MODIFY `id_hadiah` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
